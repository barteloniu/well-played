const seconds_in_a_year = 31536000
const toggle = document.querySelector("#dark-mode-toggle")

toggle.addEventListener("click", () => {
	if (document.documentElement.getAttribute("data-theme") == "dark") {
		toggle.innerHTML = "☀️"
		document.documentElement.setAttribute("data-theme", "light")
		document.cookie = "theme=light;max-age=" + seconds_in_a_year;
	} else {
		toggle.innerHTML = "🌙"
		document.documentElement.setAttribute("data-theme", "dark")
		document.cookie = "theme=dark;max-age=" + seconds_in_a_year;
	}
})
