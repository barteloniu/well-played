from flask import Blueprint, render_template
from . import api

bp = Blueprint("index", __name__)

@bp.route("/")
def index():
    return render_template("index.html", channels=api.available_channels())
