from flask import Flask

def create_app():
    app = Flask(__name__)

    from . import index
    app.register_blueprint(index.bp)

    from . import watch
    app.register_blueprint(watch.bp)

    from . import api
    app.register_blueprint(api.bp)

    return app
