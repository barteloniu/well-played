from flask import Blueprint, jsonify, Response
import requests

bp = Blueprint("api", __name__, url_prefix="/api")

user_agent = {"user-agent": "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0"}

@bp.route("/channel/<channel_slug>/manifest.mpd")
def manifest(channel_slug):
    channel_id = channel_info(channel_slug)["id"]
    url = requests.get(f"https://pilot.wp.pl/api/v1/guest/channel/{channel_id}", headers=user_agent).json()
    url = url["data"]["stream_channel"]["streams"][0]["url"][0]
    m = requests.get(url, headers=user_agent).text

    return Response(m, mimetype="application/dash+xml")


def channel_info(channel_slug):
    ac = requests.get("https://pilot.wp.pl/api/v2/guest/channels/list").json()

    for c in ac["data"]:
        if c["slug"] == channel_slug:
            return c

    return None


def available_channels():
    # get all channels and get rid of the paid ones
    ac = requests.get("https://pilot.wp.pl/api/v2/guest/channels/list").json()
    ac = filter(lambda x: x["access_status"] == "free", ac["data"])
    ac = sorted(ac, key=lambda x: x["id"])

    # get a comma seperated list od channel ids for the next step
    ids = ",".join(map(lambda x: str(x["id"]), ac))

    # grab guides for all free channels
    guide = requests.get(f"https://pilot.wp.pl/api/v2/epg?channels={ids}&limit=2").json()
    guide = sorted(guide["data"], key=lambda x: x["channel_id"])
    guide = map(lambda x: x["entries"], guide)
    guide = map(
        lambda x: x if len(x) == 2 else x + [{"title": "[brak informacji]"}],
        guide
    )

    # combine the two requests
    ac = map(lambda x: {"info": x[0], "guide": x[1]}, zip(ac, guide))

    return ac
