from flask import Blueprint, render_template
from . import api

bp = Blueprint("watch", __name__)

@bp.route("/watch/<channel_slug>")
def watch(channel_slug):
    channel = api.channel_info(channel_slug)
    return render_template("watch.html", channel=channel)
